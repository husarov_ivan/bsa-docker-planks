FROM node:12

WORKDIR /usr/src/planks

COPY package*.json ./
COPY client ./client
COPY server.js ./
COPY server ./server
COPY css ./css
COPY index.html ./

RUN npm install

CMD [ "node", "server.js" ]

EXPOSE 5000
